/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import java.net.URI;
import java.util.Objects;

public class ModInfo {
    @JsonbProperty("id")
    private final String id;
    @JsonbProperty("repo_root")
    private final URI repoRoot;

    @JsonbCreator
    public ModInfo(@JsonbProperty("id") String id,
                   @JsonbProperty("repo_root") URI repoRoot) {
        this.id = Objects.requireNonNull(id, "id");
        this.repoRoot = Objects.requireNonNull(repoRoot, "metadataSource");
    }

    public String getId() {
        return id;
    }

    public URI getRepoRoot() {
        return repoRoot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModInfo modInfo = (ModInfo) o;
        return id.equals(modInfo.id) &&
                repoRoot.equals(modInfo.repoRoot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, repoRoot);
    }
}
