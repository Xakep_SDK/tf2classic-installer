/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import javax.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

public class MetaGenerator {
    private final UpdateListener updateListener;
    private final Path root;

    public MetaGenerator(UpdateListener updateListener, Path root) {
        this.updateListener = updateListener;
        this.root = root;
    }
    
    public void saveMetadata(ModMetadata modMetadata) throws IOException {
        Path metadataFile = root.resolve("metadata.json");
        try (Writer writer = Files.newBufferedWriter(metadataFile, StandardCharsets.UTF_8)) {
            JsonbBuilder.create().toJson(modMetadata, writer);
        }
    }

    public ModMetadata generateMetadata(boolean rmOld) throws IOException, NoSuchAlgorithmException {
        List<FileData> fileData = new ArrayList<>();
        Path files = root.resolve("files");
        List<Path> paths = Files.walk(files)
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());
        updateListener.fill(paths.stream().map(Path::toString).collect(Collectors.toList()));
        for (Path path : paths) {
            fileData.add(prepareFiles(files, path, rmOld));
        }
        Properties properties = new Properties();
        try (Reader reader = Files.newBufferedReader(root.resolve("info.properties"), StandardCharsets.UTF_8)) {
            properties.load(reader);
        }
        
        String id = properties.getProperty("id");
        String version = properties.getProperty("version");
        return new ModMetadata(version, fileData);
    }

    private FileData prepareFiles(Path filesRoot, Path sourceFile, boolean rmOld) throws IOException, NoSuchAlgorithmException {
        if (!Files.isRegularFile(sourceFile)) {
            throw new IllegalArgumentException("Provided path is not a file: " + sourceFile.toAbsolutePath().toString());
        }
        long size = Files.size(sourceFile);
        String rawPathStr = filesRoot.relativize(sourceFile).toString();
        String rawHash = Utils.readHash(rawPathStr, sourceFile, updateListener);

        Path compressedPath = sourceFile.getParent().resolve(sourceFile.getFileName() + ".gzip");
        String compressedPathStr = filesRoot.relativize(compressedPath).toString();
        String compressedHash;
        try (DigestOutputStream dos = new DigestOutputStream(Files.newOutputStream(compressedPath), MessageDigest.getInstance("SHA-1"));
             OutputStream os = new GZIPOutputStream(dos)) {
            Files.copy(sourceFile, os);
            if (rmOld) {
                Files.deleteIfExists(sourceFile);
            }
            compressedHash = Utils.bytesToHex(dos.getMessageDigest().digest());
        }
        long compressedSize = Files.size(compressedPath);
        return new FileData(size, compressedSize, rawPathStr, compressedPathStr, rawHash, compressedHash);
    }
}
