/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public class Utils {
    private static final int BUF_SIZE = 4 * 1024;

    public static String readHash(String id, Path modFile, UpdateListener updateListener) throws IOException, NoSuchAlgorithmException {
        if (Files.notExists(modFile)) {
            throw new IllegalArgumentException("File not exists");
        }
        long totalSize = Files.size(modFile);
        updateListener.onUpdate(id, totalSize, 0, UpdateListener.UpdateType.HASH_START);
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        try (InputStream inputStream = Files.newInputStream(modFile);
             DigestOutputStream dos = new DigestOutputStream(OutputStream.nullOutputStream(), digest)) {
            copyWithMonitor(id, totalSize, inputStream, dos, updateListener, UpdateListener.UpdateType.HASH_START, UpdateListener.UpdateType.HASH_FINISH);
        }
        byte[] digestBytes = digest.digest();
        return bytesToHex(digestBytes);
    }

    public static void copyWithMonitor(String id,
                                       long totalSize,
                                       InputStream is,
                                       OutputStream os,
                                       UpdateListener updateListener,
                                       UpdateListener.UpdateType start,
                                       UpdateListener.UpdateType finish) throws IOException {
        byte[] buf = new byte[BUF_SIZE];
        int r;
        while ((r = is.read(buf)) != -1) {
            os.write(buf, 0, r);
            updateListener.onUpdate(id, totalSize, r, start);
        }
        updateListener.onUpdate(id, totalSize, 0, finish);
    }

    private static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hex = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; i++) {
            int b = bytes[i] & 0xFF;  // make it unsigned
            hex[i * 2] = HEX_CHARS[b >>> 4]; // take first 4 bits char
            hex[i * 2 + 1] = HEX_CHARS[b & 0x0F]; // take last 4 bits char
        }
        return new String(hex);
    }

    // most copied snipped
    public static String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %ciB", value / 1024.0, ci.current());
    }

    public static void checkPathBounds(Path root, Path child) {
        if (!child.startsWith(root)) {
            throw new SecurityException(String.format("Path out of bounds, root path: %s, child path: %s", root.toAbsolutePath().toString(), child.toAbsolutePath().toString()));
        }
    }
}
