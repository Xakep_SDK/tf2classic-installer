/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import javax.json.bind.JsonbBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.zip.GZIPInputStream;

public class ModInstaller {
    private static final int BUF_SIZE = 4 * 1024;

    private final ModInfo modInfo;
    private final DownloadUpdateListener updateListener;
    private final Path sourceModsDir;

    public ModInstaller(ModInfo modInfo, DownloadUpdateListener updateListener, Path sourceModsDir) {
        this.modInfo = Objects.requireNonNull(modInfo, "modInfo");
        this.updateListener = Objects.requireNonNull(updateListener, "updateListener");
        this.sourceModsDir = Objects.requireNonNull(sourceModsDir, "sourceModsDir");
    }

    public Path installMod(boolean checkHash) throws IOException, NoSuchAlgorithmException {
        URI resolve = modInfo.getRepoRoot().resolve("metadata.json");
        updateListener.onMetadataDownload(UpdateListener.UpdateType.DOWNLOAD);
        URLConnection urlConnection = resolve.toURL().openConnection();
        ModMetadata modMetadata;
        try (InputStream is = urlConnection.getInputStream()) {
            modMetadata = JsonbBuilder.create().fromJson(is, ModMetadata.class);
        }
        updateListener.onMetadataDownload(UpdateListener.UpdateType.DOWNLOADED);
        updateListener.fill(modMetadata);
        List<FileData> filesToDownload = getFilesToDownload(modMetadata, checkHash);
        downloadFiles(filesToDownload);
        return getModDir();
    }

    private void downloadFiles(List<FileData> fileData) throws IOException, NoSuchAlgorithmException {
        for (FileData file : fileData) {
            Path localPath = getLocalPath(file);
            Files.createDirectories(localPath.getParent());
            URI fileUri = modInfo.getRepoRoot().resolve("files/" + file.getCompressedPath());
            URLConnection urlConnection = fileUri.toURL().openConnection();
            try (RawInputStreamMonitor rism = new RawInputStreamMonitor(urlConnection.getInputStream());
                 InputStream is = new GZIPInputStream(rism);
                 DigestOutputStream os = new DigestOutputStream(Files.newOutputStream(localPath), MessageDigest.getInstance("SHA-1"))) {
                Utils.copyWithMonitor(file.getRawPath(), file.getRawSize(), is, os, updateListener, UpdateListener.UpdateType.DOWNLOAD, UpdateListener.UpdateType.DOWNLOAD);
                String hash = Utils.bytesToHex(os.getMessageDigest().digest());
                if (!hash.equalsIgnoreCase(file.getRawHash())) {
                    throw new IOException("Failed to save file, wrong hash: " + file.getRawPath());
                }
                updateListener.onUpdate(file.getRawPath(), file.getRawSize(), 0, UpdateListener.UpdateType.DOWNLOADED);
            }
        }
    }

    private List<FileData> getFilesToDownload(ModMetadata modMetadata, boolean checkHash) throws IOException, NoSuchAlgorithmException {
        if (!checkHash) {
            updateListener.skipHash();
        }

        List<FileData> toDownload = new ArrayList<>();
        for (FileData file : modMetadata.getFiles()) {
            Path modFile = getLocalPath(file);

            if (Files.notExists(modFile) || !sizeEquals(file, modFile) || (checkHash && !hashEquals(file, modFile))) {
                toDownload.add(file);
                updateListener.onUpdate(file.getRawPath(), file.getRawSize(), 0, UpdateListener.UpdateType.CHECKS_FAIL);
                continue;
            }
            updateListener.onUpdate(file.getRawPath(), file.getRawSize(), 0, UpdateListener.UpdateType.FINE);
        }
        return toDownload;
    }

    private boolean sizeEquals(FileData file, Path modFile) throws IOException {
        return Files.size(modFile) == file.getRawSize();
    }

    private boolean hashEquals(FileData file, Path modFile) throws NoSuchAlgorithmException, IOException {
        String savedHash = Utils.readHash(file.getRawPath(), modFile, updateListener);
        return file.getRawHash().equalsIgnoreCase(savedHash);
    }

    private Path getModDir() {
        if (Files.notExists(sourceModsDir)) {
            if (Files.notExists(sourceModsDir.getParent())) {
                throw new IllegalStateException("Steam not installed");
            }
        }
        return sourceModsDir.resolve(modInfo.getId());
    }

    private Path getLocalPath(FileData file) {
        Path modDir = getModDir();
        Path filePath = modDir.resolve(file.getRawPath());
        Utils.checkPathBounds(sourceModsDir, filePath);
        return filePath;
    }
}
