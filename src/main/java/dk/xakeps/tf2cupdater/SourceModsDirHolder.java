/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class SourceModsDirHolder {
    private Path directory;

    public SourceModsDirHolder() {
        if (Platform.getPlatform() == Platform.LINUX) {
            this.directory = getLinuxDir();
        } else if (Platform.getPlatform() == Platform.WINDOWS) {
            this.directory = getWindowsDir();
        }
    }

    public Optional<Path> getDirectory() {
        return Optional.ofNullable(directory);
    }

    public void setDirectory(Path directory) {
        this.directory = directory;
    }

    public boolean hasDirectory() {
        return directory != null;
    }

    public Path getDirectoryUnchecked() {
        if (directory == null) {
            throw new IllegalStateException("No sourcemods directory chosen");
        }
        return directory;
    }

    private static Path getLinuxDir() {
        return Paths.get(System.getProperty("user.home"), ".local", "share", "Steam", "steamapps", "sourcemods");
    }

    private static Path getWindowsDir() {
        try {
            String dirPath = null;
            Process exec = Runtime.getRuntime().exec("reg query HKCU\\SOFTWARE\\Valve\\Steam\\ /v SourceModInstallPath");
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(exec.getInputStream()))) {
                String s;
                while ((s = reader.readLine()) != null) {
                    if (!s.contains("SourceModInstallPath")) {
                        continue;
                    }
                    String[] split = s.split("\\s+");
                    dirPath = split[split.length - 1];
                    break;
                }
            }
            if (dirPath != null) {
                return Paths.get(dirPath);
            }
            return null;
        } catch (IOException e) {
            throw new RuntimeException("Can't get path", e);
        }
    }
}
