/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import java.util.Objects;

public class FileData {
    @JsonbProperty("raw_size")
    private final long rawSize;
    @JsonbProperty("compressed_size")
    private final long compressedSize;
    @JsonbProperty("raw_path")
    private final String rawPath;
    @JsonbProperty("compressed_path")
    private final String compressedPath;
    @JsonbProperty("raw_hash")
    private final String rawHash;
    @JsonbProperty("compressed_hash")
    private final String compressedHash;

    @JsonbCreator
    public FileData(@JsonbProperty("raw_size") long rawSize,
                    @JsonbProperty("compressed_size") long compressedSize,
                    @JsonbProperty("raw_path") String rawPath,
                    @JsonbProperty("compressed_path") String compressedPath,
                    @JsonbProperty("raw_hash") String rawHash,
                    @JsonbProperty("compressed_hash") String compressedHash) {
        this.rawSize = rawSize;
        this.compressedSize = compressedSize;
        this.rawPath = Objects.requireNonNull(rawPath, "path");
        this.compressedPath = Objects.requireNonNull(compressedPath, "compressedPath");
        this.rawHash = Objects.requireNonNull(rawHash, "rawHash");
        this.compressedHash = Objects.requireNonNull(compressedHash, "compressedHash");
    }

    public long getRawSize() {
        return rawSize;
    }

    public long getCompressedSize() {
        return compressedSize;
    }

    public String getRawPath() {
        return rawPath;
    }

    public String getCompressedPath() {
        return compressedPath;
    }

    public String getRawHash() {
        return rawHash;
    }

    public String getCompressedHash() {
        return compressedHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileData fileData = (FileData) o;
        return rawPath.equals(fileData.rawPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rawPath);
    }
}
