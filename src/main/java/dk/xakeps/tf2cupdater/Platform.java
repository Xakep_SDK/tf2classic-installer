/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import java.util.Locale;

public enum Platform {
    LINUX("linux", "unix"),
    OSX("osx", "mac"),
    WINDOWS("windows"),
    UNKNOWN("unknown");

    private static final Platform CURRENT_PLATFORM;

    static {
        CURRENT_PLATFORM = getByName(System.getProperty("os.name"));
    }

    private final String[] aliases;

    Platform(String... aliases) {
        this.aliases = aliases;
    }

    public static Platform getPlatform() {
        return CURRENT_PLATFORM;
    }

    public static Platform getByName(String osName) {
        osName = osName.toLowerCase(Locale.ROOT);
        for (Platform os : Platform.values()) {
            for (String alias : os.aliases) {
                if (osName.contains(alias)) {
                    return os;
                }
            }
        }
        return UNKNOWN;
    }
}
