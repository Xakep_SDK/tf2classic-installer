/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import java.util.List;
import java.util.Objects;

public class ModMetadata {
    @JsonbProperty("version")
    private final String version;
    @JsonbProperty("files")
    private final List<FileData> files;

    @JsonbCreator
    public ModMetadata(@JsonbProperty("version") String version,
                       @JsonbProperty("files") List<FileData> files) {
        this.version = version;
        this.files = Objects.requireNonNull(files, "files");
    }

    public String getVersion() {
        return version;
    }

    public List<FileData> getFiles() {
        return files;
    }
}
