/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater;

import java.util.List;

public class TerminalUpdateListener implements DownloadUpdateListener {
    private int total;
    private int downloaded;

    @Override
    public void onUpdate(String id, long total, long read, UpdateListener.UpdateType updateType) {
        if (updateType == UpdateListener.UpdateType.HASH_FINISH || updateType == UpdateType.FINE || updateType == UpdateType.DOWNLOADED) {
            System.out.printf("%d / %d %s\n", ++downloaded, this.total, id);
        }
    }

    @Override
    public void fill(List<String> files) {
        total = files.size();
    }

    @Override
    public void onMetadataDownload(UpdateType updateType) {
        switch (updateType) {
            case DOWNLOAD:
                System.out.println("Metadata downloading");
                break;
            case DOWNLOADED:
                System.out.println("Metadata downloaded");
                break;
        }
    }

    @Override
    public void skipHash() {

    }

    @Override
    public void fill(ModMetadata modMetadata) {
        total = modMetadata.getFiles().size();
    }
}
