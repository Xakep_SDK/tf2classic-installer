/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater.gui;

import dk.xakeps.tf2cupdater.Platform;
import dk.xakeps.tf2cupdater.UpdateListener;
import dk.xakeps.tf2cupdater.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class Quirks {
    private static final Map<Platform, List<Quirks>> QUIRKS;

    static {
        QUIRKS = new HashMap<>();
        List<Quirks> linuxQuirks = new ArrayList<>();
        QUIRKS.put(Platform.LINUX, linuxQuirks);
        linuxQuirks.add(
                new Quirks(
                        Platform.LINUX,
                        "On GNU/Linux you need to copy files from vpks directory to custom. Do it for you?",
                        true,
                        (path, updateListener) -> {
                            Path vpks = path.resolve("vpks");
                            List<Path> collect = Files.walk(vpks)
                                    .filter(Files::isRegularFile)
                                    .collect(Collectors.toList());
                            List<String> paths = collect.stream()
                                    .map(path::relativize)
                                    .map(Path::toString)
                                    .collect(Collectors.toList());
                            updateListener.fill(paths);
                            Path dest = path.resolve("custom");
                            for (Path src : collect) {
                                Path target = dest.resolve(vpks.relativize(src));
                                String id = path.relativize(src).toString();
                                try (InputStream is = Files.newInputStream(src);
                                     OutputStream os = Files.newOutputStream(target)) {
                                    long size = Files.size(src);
                                    Utils.copyWithMonitor(id, size, is, os, updateListener, UpdateListener.UpdateType.COPY_START,  UpdateListener.UpdateType.COPY_FINISH);
                                }
                            }
                        }
                )
        );
        linuxQuirks.add(
                new Quirks(
                        Platform.LINUX,
                        "You need to add '-steam -steam' in launch options, yes, 2 times, otherwise VAC will not work",
                        false,
                        (path, updateListener) -> {}
                )
        );
    }

    private final Platform platform;
    private final String message;
    private final boolean automatic;
    private final IOBiConsumer<Path, UpdateListener> action;

    private Quirks(Platform platform, String message, boolean automatic, IOBiConsumer<Path, UpdateListener> action) {
        this.platform = Objects.requireNonNull(platform, "platform");
        this.message = Objects.requireNonNull(message, "message");
        this.automatic = automatic;
        this.action = Objects.requireNonNull(action, "action");
    }

    public static List<Quirks> getQuirks() {
        return QUIRKS.getOrDefault(Platform.getPlatform(), Collections.emptyList());
    }

    public String getMessage() {
        return message;
    }

    public boolean isAutomatic() {
        return automatic;
    }

    public void apply(Path modDirectory, UpdateListener updateListener) throws IOException {
        action.accept(modDirectory, updateListener);
    }

    @FunctionalInterface
    private interface IOBiConsumer<F, S> {
        void accept(F f, S s) throws IOException;
    }
}
