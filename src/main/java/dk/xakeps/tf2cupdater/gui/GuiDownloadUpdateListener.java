/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater.gui;

import dk.xakeps.tf2cupdater.DownloadUpdateListener;
import dk.xakeps.tf2cupdater.FileData;
import dk.xakeps.tf2cupdater.ModInfo;
import dk.xakeps.tf2cupdater.ModMetadata;
import dk.xakeps.tf2cupdater.gui.download.DownloadPanel;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;

public class GuiDownloadUpdateListener implements DownloadUpdateListener {
    private final ModInfo modInfo;
    private final DownloadPanel downloadPanel;

    public GuiDownloadUpdateListener(ModInfo modInfo, DownloadPanel downloadPanel) {
        this.modInfo = Objects.requireNonNull(modInfo, "modInfo");
        this.downloadPanel = Objects.requireNonNull(downloadPanel, "downloadPanel");
    }

    @Override
    public void onMetadataDownload(UpdateType updateType) {
        SwingUtilities.invokeLater(() -> {
            switch (updateType) {
                case DOWNLOAD:
                    downloadPanel.setStatus("Metadata download");
                    break;
                case DOWNLOADED:
                    downloadPanel.setStatus("Metadata downloaded");
                    break;
            }
        });
    }

    @Override
    public void onUpdate(String id, long total, long read, UpdateType updateType) {
        SwingUtilities.invokeLater(() -> {
            downloadPanel.updateItem(id, read, updateType);
            downloadPanel.setStatus("Current file: " + id + ", status: " + updateType.toString());
        });
    }

    @Override
    public void fill(List<String> files) {
        try {
            SwingUtilities.invokeAndWait(() -> {
                downloadPanel.clear();
                for (String file : files) {
                    downloadPanel.addItem(file);
                }
            });
        } catch (InterruptedException | InvocationTargetException e) {
            throw new RuntimeException("Fill error", e);
        }
    }

    @Override
    public void skipHash() {
        SwingUtilities.invokeLater(() -> {
            downloadPanel.setStatus("Hash checks skipped");
            downloadPanel.skipHash();
        });
    }

    @Override
    public void fill(ModMetadata modMetadata) {
        try {
            SwingUtilities.invokeAndWait(() -> {
                downloadPanel.clear();
                for (FileData file : modMetadata.getFiles()) {
                    downloadPanel.addItem(file);
                }
            });
        } catch (InterruptedException | InvocationTargetException e) {
            throw new RuntimeException("Fill error", e);
        }
    }
}
