/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater.gui;

import dk.xakeps.tf2cupdater.ModInfo;

import javax.swing.*;
import java.awt.*;

public class Gui {
    private final JFrame frame;

    public Gui(ModInfo modInfo) {
        frame = new JFrame("Mod installer 1.0");
        frame.setSize(768, 512);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainPanel mainPanel = new MainPanel(modInfo, this);
        changePanel(mainPanel);
//        frame.pack();
//        frame.setVisible(true);
//        frame.setLocationRelativeTo(null);
    }

    protected void changePanel(Container container) {
        frame.getContentPane().removeAll();
        frame.getContentPane().add(container);
    }

    protected void pack() {
        frame.pack();
    }

    protected void recalculate() {
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    public void start() {
        recalculate();
        frame.setVisible(true);
    }
}
