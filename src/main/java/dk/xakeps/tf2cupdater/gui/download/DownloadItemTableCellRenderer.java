/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater.gui.download;

import dk.xakeps.tf2cupdater.Utils;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Objects;

public class DownloadItemTableCellRenderer extends JPanel implements TableCellRenderer {
    private final RenderElement renderElement;
    private final JLabel label;
    private final JProgressBar progressBar;

    public DownloadItemTableCellRenderer(RenderElement renderElement) {
        super(new FlowLayout());
        this.renderElement = Objects.requireNonNull(renderElement, "renderElement");
        switch (renderElement) {
            case PROGRESS_BAR:
                label = null;
                progressBar = new JProgressBar();
                progressBar.setStringPainted(true);
                add(progressBar);
                break;
            case STATUS:
            case NAME:
            case BYTES:
                progressBar = null;
                label = new JLabel();
                add(label);
                break;
            default:
                throw new IllegalArgumentException("Wrong renderElement");
        }
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if(isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        DownloadItem item = (DownloadItem) value;
        switch (renderElement) {
            case PROGRESS_BAR:
                if (item.getTotal() < 0) {
                    if (item.getDownloaded() < 0) {
                        progressBar.setIndeterminate(false);
                        progressBar.setValue(100);
                        progressBar.setString(item.getUpdateType().toString());
                    } else {
                        progressBar.setIndeterminate(true);
                        progressBar.setString(Utils.humanReadableByteCountBin(item.getDownloaded()));
                    }
                } else {
                    int val = (int) Math.ceil((((float) item.getDownloaded() / (float) item.getTotal()) * 100f));
                    progressBar.setValue(val);
                    progressBar.setString(Utils.humanReadableByteCountBin(item.getDownloaded()) + " / " + Utils.humanReadableByteCountBin(item.getTotal()));
                }
                break;
            case NAME:
                label.setText(item.getId());
                break;
            case BYTES:
                if (item.getTotal() < 0) {
                    label.setText(String.valueOf(item.getDownloaded()));
                } else {
                    label.setText(item.getDownloaded() + " / " + item.getTotal());
                }
                break;
            case STATUS:
                label.setText(item.getUpdateType().toString());
                break;
        }
        return this;
    }

    enum RenderElement {
        PROGRESS_BAR, NAME, BYTES, STATUS
    }
}
