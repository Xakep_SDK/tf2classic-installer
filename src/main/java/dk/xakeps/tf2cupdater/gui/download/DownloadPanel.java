/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater.gui.download;

import dk.xakeps.tf2cupdater.FileData;
import dk.xakeps.tf2cupdater.ModInfo;
import dk.xakeps.tf2cupdater.ModInstaller;
import dk.xakeps.tf2cupdater.UpdateListener;
import dk.xakeps.tf2cupdater.Utils;
import dk.xakeps.tf2cupdater.gui.GuiDownloadUpdateListener;
import dk.xakeps.tf2cupdater.gui.Quirks;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

public class DownloadPanel extends JPanel {
    private final JLabel status;
    private final Map<String, DownloadItem> data;
    private final DefaultTableModel tableModel;

    private final JTable table;

    private final JProgressBar progressBar;

    private final ModInfo modInfo;

    private long totalSize;
    private long downloadedSize;
    private long lastSpeedCheck;
    private long speed;
    private long speedView;

    public DownloadPanel(ModInfo modInfo) {
        this.modInfo = Objects.requireNonNull(modInfo, "modInfo");
        setLayout(new BorderLayout());
        this.status = new JLabel("Nothing");
        this.data = new HashMap<>();
        add(status, BorderLayout.PAGE_START);
        status.setHorizontalAlignment(JLabel.CENTER);

        this.tableModel = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return DownloadItem.class;
            }
        };
        this.table = new JTable(tableModel);

        tableModel.addColumn("ID");
        tableModel.addColumn("Progress");
        tableModel.addColumn("Status");
        tableModel.addColumn("Bytes");
        table.createDefaultColumnsFromModel();
        table.getColumn("ID").setCellRenderer(new DownloadItemTableCellRenderer(DownloadItemTableCellRenderer.RenderElement.NAME));
        table.getColumn("Progress").setCellRenderer(new DownloadItemTableCellRenderer(DownloadItemTableCellRenderer.RenderElement.PROGRESS_BAR));
        table.getColumn("Status").setCellRenderer(new DownloadItemTableCellRenderer(DownloadItemTableCellRenderer.RenderElement.STATUS));
        table.getColumn("Bytes").setCellRenderer(new DownloadItemTableCellRenderer(DownloadItemTableCellRenderer.RenderElement.BYTES));

        table.setRowHeight(30);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);

        add(new JScrollPane(table), BorderLayout.CENTER);

        this.progressBar = new JProgressBar();
        progressBar.setStringPainted(true);
        add(progressBar, BorderLayout.PAGE_END);
    }

    public void clear() {
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            tableModel.removeRow(i);
        }
    }

    public void addItem(String item) {
        addItem(new DownloadItem(item, -1));
    }

    public void addItem(FileData fileData) {
        totalSize += fileData.getRawSize();
        DownloadItem item = new DownloadItem(fileData.getRawPath(), fileData.getRawSize());
        addItem(item);
    }

    public void addItem(DownloadItem downloadItem) {
        Vector<Object> v = new Vector<>();
        v.add(downloadItem);
        v.add(downloadItem);
        v.add(downloadItem);
        v.add(downloadItem);
        tableModel.addRow(v);
        data.put(downloadItem.getId(), downloadItem);
    }

    public void updateItem(String id, long downloaded, UpdateListener.UpdateType updateType) {
        DownloadItem item = data.get(id);
        item.addDownloaded(downloaded);
        item.setUpdateType(updateType);
        downloadedSize += downloaded;
        int val = (int) Math.ceil((((float) downloadedSize / (float) totalSize) * 100f));
        progressBar.setValue(val);
        speed += downloaded;
        if (lastSpeedCheck + TimeUnit.SECONDS.toMillis(1) < System.currentTimeMillis()) {
            lastSpeedCheck = System.currentTimeMillis();
            // update speed;
            speedView = speed;
            speed = 0;
        }
        progressBar.setString(Utils.humanReadableByteCountBin(downloadedSize) + " / " + Utils.humanReadableByteCountBin(totalSize) + ", " + Utils.humanReadableByteCountBin(speedView) + "/sec");
        if (updateType == UpdateListener.UpdateType.DOWNLOADED || updateType == UpdateListener.UpdateType.FINE) {
            remove(item);
        }
        table.repaint();
    }

    // shrug
    private void remove(DownloadItem item) {
        if (item == null) return;
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            if (item.equals(tableModel.getValueAt(i, 0))) {
                tableModel.removeRow(i);
                return;
            }
        }
    }

    public void setStatus(String status) {
        this.status.setText(status);
    }

    public void skipHash() {
        for (DownloadItem value : data.values()) {
            value.setDownloaded(value.getTotal());
            value.setUpdateType(UpdateListener.UpdateType.SKIP_HASH);
        }
    }

    public void install(boolean checkHash, Path sourceModsDir) throws IOException, NoSuchAlgorithmException {
        GuiDownloadUpdateListener listener = new GuiDownloadUpdateListener(modInfo, this);
        ModInstaller installer = new ModInstaller(modInfo, listener, sourceModsDir);
        Path modDir = installer.installMod(checkHash);
        finish(modDir, listener);
    }

    private void finish(Path modDir, UpdateListener listener) throws IOException {
        JOptionPane.showMessageDialog(this, "All files installed successfully");
        List<Quirks> quirks = Quirks.getQuirks();
        if (quirks.isEmpty()) {
            System.exit(0);
        }
        int applyQuirks = JOptionPane.showConfirmDialog(this, "There are " + quirks.size() + " quirks needed to be fixed. Fix one by one?");
        if (applyQuirks != JOptionPane.YES_OPTION) {
            System.exit(0);
        }
        for (Quirks quirk : Quirks.getQuirks()) {
            if (quirk.isAutomatic()) {
                int applyQuirk = JOptionPane.showConfirmDialog(this, quirk.getMessage());
                if (applyQuirk != JOptionPane.YES_OPTION) {
                    continue;
                }
                quirk.apply(modDir, listener);
            } else {
                JOptionPane.showMessageDialog(this, quirk.getMessage());
            }
        }
    }
}
