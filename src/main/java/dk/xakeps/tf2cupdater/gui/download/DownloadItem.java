/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater.gui.download;

import dk.xakeps.tf2cupdater.UpdateListener;

import java.util.Objects;

public class DownloadItem {
    private final String id;
    private final long total;
    private long downloaded;
    private UpdateListener.UpdateType updateType;

    public DownloadItem(String id, long total) {
        this.id = Objects.requireNonNull(id, "id");
        this.total = total;
        this.updateType = UpdateListener.UpdateType.NEW;
    }

    public String getId() {
        return id;
    }

    public long getTotal() {
        return total;
    }

    public long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(long downloaded) {
        this.downloaded = downloaded;
    }

    public void addDownloaded(long add) {
        this.downloaded += add;
    }

    public UpdateListener.UpdateType getUpdateType() {
        return updateType;
    }

    public void setUpdateType(UpdateListener.UpdateType updateType) {
        this.updateType = updateType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DownloadItem that = (DownloadItem) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
