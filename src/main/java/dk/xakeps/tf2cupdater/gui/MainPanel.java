/*
    Copyright (C) 2020 Xakep_SDK

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.tf2cupdater.gui;

import dk.xakeps.tf2cupdater.ModInfo;
import dk.xakeps.tf2cupdater.SourceModsDirHolder;
import dk.xakeps.tf2cupdater.gui.download.DownloadPanel;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class MainPanel extends JPanel {
    private final JFileChooser fileChooser;
    private final SourceModsDirHolder dirHolder;

    public MainPanel(ModInfo modInfo, Gui gui) {
        Objects.requireNonNull(modInfo, "modInfo");
        Objects.requireNonNull(gui, "gui");
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        this.dirHolder = new SourceModsDirHolder();

        JLabel dirLabel = new JLabel(dirHolder.getDirectory().map(Path::toString).orElse("No directory found"));

        add(dirLabel);
        dirLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);

        ControlsPanel panel = new ControlsPanel(modInfo, gui, dirLabel);
        add(panel);

        if (!dirHolder.hasDirectory()) {
            JOptionPane.showMessageDialog(this, "Can't find sourcemods directory. Select it manually");
            forceChooseDir();
        }
        panel.install.setEnabled(true);
    }

    private void forceChooseDir() {
        int i = fileChooser.showOpenDialog(this);
        if (i != JFileChooser.APPROVE_OPTION) {
            forceChooseDir();
        }
        dirHolder.setDirectory(fileChooser.getSelectedFile().toPath());
    }

    private class ControlsPanel extends JPanel {
        private final JButton install;

        public ControlsPanel(ModInfo modInfo, Gui gui, JLabel dirLabel) {
            super(new FlowLayout());

            JCheckBox checkHash = new JCheckBox("Check hashes");
            checkHash.setSelected(true);
            add(checkHash);


            this.install = new JButton("Install");
            install.setEnabled(false);
            install.addActionListener(e -> {
                DownloadPanel panel = new DownloadPanel(modInfo);
                gui.changePanel(panel);
                gui.recalculate();
                new Thread(() -> {
                    try {
                        panel.install(checkHash.isSelected(), dirHolder.getDirectoryUnchecked());
                        System.exit(0);
                    } catch (IOException | NoSuchAlgorithmException ex) {
                        throw new RuntimeException("Can't install", ex);
                    }
                }).start();
            });
            add(install);
            JButton selectDir = new JButton("Select directory");
            selectDir.addActionListener(e -> {
                int i = fileChooser.showOpenDialog(this);
                if (i == JFileChooser.APPROVE_OPTION) {
                    dirHolder.setDirectory(fileChooser.getSelectedFile().toPath());
                    dirLabel.setText(dirHolder.getDirectoryUnchecked().toString());
                }
            });
            add(selectDir);
        }
    }
}
