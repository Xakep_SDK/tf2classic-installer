### This is simple installer and updater for TF2Classic

## How it works
1. It loads some metadata from mod_info.json  
mod_info.json contains mod id (tf2classic) and repository URL
2. It loads more metadata from repo URL (repo_url/metadata.json)  
metadata.json contains remote mod version and mod files
3. It tries to find sourcemods directory, if it can't find it, it will request you to select it
4. When you click install, it will do these checks:  
4.1. Check if file exists  
4.2. Check file size  
4.3. Check file hash (optional, can be disabled in main menu)
5. It will download missing files and re-download corrupt files

## How to host your own server for files
All you need is just a web server
1. Go to web server directory
2. Create file `info.properties` with this contents:
```
id=tf2classic # mod id
version=2.0.0 # mod version
```
3. Create directory `files`
4. Upload source mod in `files` directory  
This directory should contain `gameinfo.txt` file
5. Upload this installer and place it near `info.properties` file
6. Launch it using this command `java -jar installer.jar meta`  
This will generate `metadata.json` file and compress mod files
7. Change links in your installer copy to point to a web server

Example file structure after generating metadata:
```
├── index.html
└── tf2c
    ├── files
    │   ├── gameinfo.txt.gzip
    │   ├── tf2c_base.fgd.gzip
    │   ├── tf2c.fgd.gzip
    │   ├── TF2ClassicLauncher.exe.gzip
    │   ├── thirdpartycredits.txt.gzip
    │   └── version.txt.gzip
    ├── info.properties
    ├── metadata.json
    └── tf2cupdater-1.0-SNAPSHOT.jar
```
Repository root: `https://xakeps.dk/tf2c/`